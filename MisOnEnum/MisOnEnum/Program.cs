﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnEnum
{
    enum Mast { Risti = 1, Ruutu, Ärtu, Poti, Pada = 4}

    enum Tugevus { Kaks = 2, Kolm, Neli, Viis, Kuus, Seitse, Kaheksa, Üheksa, Kümme, Soldat, Emand, Kuningas, Äss}
    [Flags] enum Kirjeldus { Puust = 1, Punane = 2, Suur = 4, Kolisev = 8}

    enum Sugu { Naine = 0, Mees = 1, Tundmata = 99}

    struct Kaart {
        public Mast mast; public Tugevus tugevus;
        public override string ToString()
        {
            return $"{mast} {tugevus}";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Console.Write("mis on trump: ");
            //Mast m = (Mast)Enum.Parse(typeof(Mast), Console.ReadLine());

            //Console.WriteLine(m);

            //Tugevus t = Tugevus.Emand;
            //Kaart k = new Kaart { mast = Mast.Risti, tugevus = Tugevus.Emand };

            //Console.WriteLine(k);

            Kirjeldus kir = Kirjeldus.Punane | Kirjeldus.Suur;
            Console.WriteLine(kir);
            kir |= Kirjeldus.Kolisev;
            Console.WriteLine(kir);
            Console.WriteLine((int)kir);
            
            // misasi on enum



        }
    }
}
