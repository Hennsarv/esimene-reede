﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaardipakiSegamine
{
    class Program
    {
        enum Mast  {Risti, Ruutu, Ärtu, Poti }
        enum Kaart {Kaks, Kolm, Neli, Viis, Kuus, Seitse, Kaheksa, Üheksa, Kümme, Soldat, Emand, Kuningas, Äss }

        static void Main(string[] args)
        {
            /*
             * 
            Sul on massiiv 52 elementi
            See oleks vaja täita juhulike kaartidega 
            (52 kaarti vaja segada)
            trükkida välja neljas tulbas (bridži käsi)
            
            

            */
            // kuidas trrükkida kaart number 27
            int kaart = 27;
            Console.WriteLine($"{(Mast)(kaart / 13)} {(Kaart)(kaart % 13)}");
            // kaarte on 52 - 0..12 on potid, 13..25 ärtud jne
            // kaart / 13 (NB! meil on int) annab väärtuse 0..3
            // kaart % 13 annab väärtuse 0..12
            // teisendades (cast) selle inti Mastiks ja Kaardiks saamegi
            // SAMA VÕIKS TEHA KA MASSIIVIGA, kus vastavalt 4 ja 13 elementi

            //int[] segamata = Enumerable.Range(0, 52).ToArray();

            int[] segamata = new int[52];
            for (int i = 0; i < segamata.Length; i++) segamata[i] = i;
            
            // kuidas segada massiivi
            // hetkel ei oska ma paremini, kui käsitsi
            // võtan kaks suvalist kaarti ja vahetan nad ära
            // teen seda küllalt mitu korda

            Random r = new Random(); // saan juhuslikke arve siit
            for(int i = 0; i < 1000000; i++) // nii saan miljon korda
                                    // võibolla piisab ka tuhandest :)
            {
                int üks = r.Next(52); // 0 <= üks < 52
                int teine = r.Next(52); // 0 <= teine < 52
                int vahepeal = segamata[üks]; // üks kaart taskusse
                segamata[üks] = segamata[teine]; // teine kaart ühe kohale
                segamata[teine] = vahepeal; // // taskust teise kohale
            }

            // trükin välja numbrid 13 tükki reas
            for (int i = 0; i < segamata.Length; i++)
                Console.Write(
                    (i % 13 == 0 ? "\n" : "") // iga 13 tagant 1 reavahetus  
                    +
                    $"{segamata[i]} "); // vastav number massiivist
            Console.WriteLine(); // lõppu veel 1 reavahetus

            // trükin välja kaardid 4 tükki reas
            for (int i = 0; i < segamata.Length; i++) 
                Console.Write(
                    (i % 4 == 0 ? "\n" : "")  // iga 4 tagant reavahetus
                    + $"{(Mast)(segamata[i] / 13)} {(Kaart)(segamata[i] % 13)}\t"
                    // kaardi nimi nagu eespool kirjeldatud
                    );

            Console.WriteLine();

            List<int> segamataPakk = Enumerable.Range(0,52).ToList();
            List<int> segatudPakk = new List<int>();
            while (segamataPakk.Count > 0)
            {
                int i = r.Next(segamataPakk.Count);
                int k = segamataPakk[i];
                segatudPakk.Add(k);
                segamataPakk.Remove(k);
            }

        }
    }
}
