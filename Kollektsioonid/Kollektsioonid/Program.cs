﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kollektsioonid
{
    class Program
    {
        static void Main(string[] args)
        {
            // massiiv ei ole ainuke
            // aga ta on kõige lihtsam
            int[] arvudeMassiiv = { 1, 2, 3, 2, 5, 1, 3, 7, 2, 1, 8 };

            // list on üsna palju paindlikum
            // sinna saab lisada
            // sealt saab eemaldada
            List<int> arvudeList = new List<int>(100) { 7, 8, 9, 1, 2, 3 };

            arvudeList.Add(7); Console.WriteLine($"{arvudeList.Count} of {arvudeList.Capacity}");
            arvudeList.Add(3); Console.WriteLine($"{arvudeList.Count} of {arvudeList.Capacity}");
            arvudeList.Add(18); Console.WriteLine($"{arvudeList.Count} of {arvudeList.Capacity}");
            arvudeList.Add(7); Console.WriteLine($"{arvudeList.Count} of {arvudeList.Capacity}");
            arvudeList.Add(3); Console.WriteLine($"{arvudeList.Count} of {arvudeList.Capacity}");
            arvudeList.Add(18); Console.WriteLine($"{arvudeList.Count} of {arvudeList.Capacity}");
            arvudeList.Add(7); Console.WriteLine($"{arvudeList.Count} of {arvudeList.Capacity}");
            arvudeList.Add(3); Console.WriteLine($"{arvudeList.Count} of {arvudeList.Capacity}");
            arvudeList.Add(18); Console.WriteLine($"{arvudeList.Count} of {arvudeList.Capacity}");

            arvudeList.Remove(3); Console.WriteLine($"{arvudeList.Count} of {arvudeList.Capacity}");

            foreach (int x in arvudeList) Console.WriteLine(x);
            for (int i = 0; i < arvudeList.Count; i++)
            {
                Console.WriteLine(arvudeList[i]);
            }

            // viimane vigur listiga
            arvudeList = arvudeMassiiv.ToList();
            arvudeMassiiv = arvudeList.ToArray();

            // LIstis ja massiivis on korduvaid tegelasi
            Console.WriteLine("\nsiit hakkab set - sorted set\n");
            SortedSet<int> arvudeSet = new SortedSet<int>();
            arvudeSet.Add(7);
            arvudeSet.Add(7);
            arvudeSet.Add(2);
            arvudeSet.Add(5);
            arvudeSet.Add(2);

            Console.WriteLine(arvudeSet.Count);
            foreach (int x in arvudeSet) Console.WriteLine(x);

            Console.WriteLine("\n siit algavad queue ja stack\n");
            Queue<int> arvudeJärjekord = new Queue<int>();
            arvudeJärjekord.Enqueue(3);
            arvudeJärjekord.Enqueue(5);
            arvudeJärjekord.Enqueue(7);
            arvudeJärjekord.Enqueue(1);
            arvudeJärjekord.Enqueue(2);

            Console.WriteLine($"järgmine oleks {arvudeJärjekord.Peek()}");

            while (arvudeJärjekord.Count > 0)
                Console.WriteLine(arvudeJärjekord.Dequeue());
            Console.WriteLine("\nenne queue ja nüüd stack\n");
            Stack<int> arvudeKuhi = new Stack<int>();
            arvudeKuhi.Push(3);
            arvudeKuhi.Push(5);
            arvudeKuhi.Push(7);
            arvudeKuhi.Push(1);
            arvudeKuhi.Push(2);


            Console.WriteLine($"järgmine oleks {arvudeKuhi.Peek()}");

            while (arvudeKuhi.Count > 0)
                Console.WriteLine(arvudeKuhi.Pop());

            // kõikse toredam (aga sellega ei piirduta)
            Console.WriteLine("\nDictionary\n");

            Dictionary<string, int> inimesed = new Dictionary<string, int>();
            inimesed.Add("Henn", 63);
            inimesed.Add("Ants", 40);
            inimesed.Add("Peeter", 28);
            inimesed.Add("Joosep", 17);
            inimesed.Add("Malle", 22);

            Console.WriteLine($"Peetri vanus on {inimesed["Peeter"]}");

            Console.WriteLine("\nDictionary foreach\n");
            foreach (var x in inimesed) Console.WriteLine($"{x.Key} vanus on {x.Value}");

            Console.WriteLine("\nDictionary foreach võtmed\n");

            foreach (string x in inimesed.Keys)
                Console.WriteLine($"{x} vanus on {inimesed[x]}");

            Console.WriteLine("\nDictionary foreach values\n");

            foreach (int x in inimesed.Values) Console.WriteLine(x);

            Console.WriteLine($"Keskmine vanus on {inimesed.Values.Average()}");

            // õpilased ja nende hinded
            // nimi hindeid on palju

            Dictionary<string, List<int>> ÕpilasteHinded = new Dictionary<string, List<int>>();

            ÕpilasteHinded.Add("Henn", new List<int>());
            ÕpilasteHinded["Henn"].Add(5);
            ÕpilasteHinded["Henn"].Add(2);
            ÕpilasteHinded["Henn"].Add(7);
            ÕpilasteHinded.Add("Ants", new List<int>());
            ÕpilasteHinded["Ants"].Add(3);
            ÕpilasteHinded["Ants"].Add(2);
            ÕpilasteHinded["Ants"].Add(4);
            ÕpilasteHinded.Add("Peeter", new List<int>());


            foreach (string n in ÕpilasteHinded.Keys)
                Console.WriteLine($"{n} keskmine hinne on {(ÕpilasteHinded[n].Count == 0 ? 0 : ÕpilasteHinded[n].Average())} ");


        }
    }
}
