﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReedeKordamine
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine('0'+0);
            Console.Write("Anna nimi: ");
            string nimi = Console.ReadLine();

            //HENN SARV -> Henn Sarv
            // henn SArv -> Henn Sarv
            // sul läheb vaja kolme funktsiooni
            //      nimi.split
            //      x.ToUpper() või ToLower()
            //      string.Join(
            // ehk kuidas viia nimi normaalkujul(Title Case e.suured algustähed)
            // anna endast märku, kui sul valmis!

            string[] nimed = nimi.Split(' ');
            // siin vahel tuleks nimedega midagi teha
            for (int i = 0; i < nimed.Length; i++)
            {
                if (nimed[i] != "")
                nimed[i] = nimed[i].Substring(0, 1).ToUpper()
                         + nimed[i].Substring(1).ToLower();
            }


            nimi = string.Join(" ", nimed);
            Console.WriteLine($"Korralik nimi on: \"{nimi}\"");

            // TEINE VARIANT
            string[] nimed2 = nimi.Split(' ');
            string uusnimi = "";
            foreach (string tykk in nimed2)
                if (tykk != "")
                uusnimi += tykk.Substring(0, 1).ToUpper()
                        + tykk.Substring(1).ToLower() + " ";

            Console.WriteLine($"Korralik nimi on: \"{uusnimi.Trim()}\"");

            // KOLMAS VARIANT ON KA - seda oskame kuskil II nädala lõpus
            string kolmasnimi = string.Join(" ", nimi
                    .Split(' ')
                    .Where(x => x != "")
                    .Select(x => x.Substring(0, 1).ToUpper() 
                                + x.Substring(1).ToLower()));
            Console.WriteLine($"Korralik nimi on: \"{kolmasnimi}\"");



            // KUI SA SELLEGA VALMIS SAAD

            string isikukood = "35503070211";
            // string isikukood = Console.ReadLine();
            // sünniaasta = kui isikukood[0] == 1 või 2, siis 18xx
            //            = kui isikukood[0] == 3 või 4, siis 19xx
            //            = kui isikukood[0] == 5 või 6, siis 20xx
            //          xx = isikukood.Substring(1,2)
            // sünnikuupäev = DateTime.Parse( .... ) ???
            DateTime sünnikuupäev = DateTime.Parse($"{isikukood.Substring(5, 2)}.{isikukood.Substring(3, 2)}.{(isikukood[0] - 1) / 2 - 6}{isikukood.Substring(1, 2)}");
            Console.WriteLine($"\nisikukood on {isikukood}");
            Console.WriteLine($"sünnikuupäev on {sünnikuupäev}");
            Console.WriteLine("sünnipäev on {?}");
            // sinu ülesanne on leida isikukoodist
            // sünnikuupäev, sünnipäev sellel aastal ja vanus
            // siin sul läheb vaja DateTime.Parse(string kuupäevakujul) 
            // või new DateTime(int aasta, int kuu, int päev)
            // ja sünnipäev. <- siin näed kõiki "tehteid" 
            // sa peaks leiutama!
            // PALUN KONTROLLI et sinu lahendus töötaks selliste isikukoodidega
            // 35503070211 (mina) 48211110000, 50002290000
            // ja annaks ÕIGE tulemuse
            // (NB! seda viimast nimetatakse testimiseks :))

            // see arvutab kahe kuupäeva vahe
            // see vahe on TimeSpan tüüpi
            TimeSpan vahe = DateTime.Today - sünnikuupäev;

            // see on üks võimalus vanust arvutada
            int vanus1 = vahe.Days * 4 / 1461;

            // see on teine võimalus vanust arvutada
            int vanus = DateTime.Now.Year - sünnikuupäev.Year;
            if (sünnikuupäev.AddYears(vanus) > DateTime.Today) vanus--;

            Console.WriteLine($"vanus on {vanus}");

        }
    }
}
